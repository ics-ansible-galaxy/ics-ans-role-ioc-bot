# ics-ans-role-ioc-bot

Ansible role to install ioc-bot, a GitLab bot that handles IOC repositories

## Role Variables

```yaml
ioc_bot_network: ioc-bot-network
ioc_bot_redis_tag: 4.0
ioc_bot_image: registry.esss.lu.se/ics-infrastructure/ioc-bot
ioc_bot_tag: latest
# User private ssh key to clone and push to GitLab
ioc_bot_ssh_key: "private ssh key"
# GitLab webhook secret
ioc_bot_gl_secret: "secret"
# GitLab token to access the API
ioc_bot_gl_access_token: "mytoken"
ioc_bot_frontend_rule: "Host:{{ ansible_fqdn }}"
# Sentry Data Source Name (error reporting)
ioc_bot_sentry_dsn: ""
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ioc-bot
```

## License

BSD 2-clause
