import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_ioc_bot_containers(host):
    with host.sudo():
        cmd = host.command('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.strip().split('\n')[1:]])
    assert names == ['ioc_bot_redis', 'ioc_bot_web', 'ioc_bot_worker', 'traefik_proxy']


def test_ioc_bot_index(host):
    # This tests that traefik forwards traffic to the ioc-bot web server
    # and that the web server is online
    cmd = host.command('curl -H Host:ics-ans-role-ioc-bot-default -k -L https://localhost/health')
    assert 'Bot OK' in cmd.stdout
